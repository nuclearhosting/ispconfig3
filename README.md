# ISPConfig - Hosting Control Panel

Nightly (master): [![pipeline status](https://git.ispconfig.org/ispconfig/ispconfig3/badges/master/pipeline.svg)](https://git.ispconfig.org/ispconfig/ispconfig3/commits/master)   
Stable branch: [![pipeline status](https://git.ispconfig.org/ispconfig/ispconfig3/badges/stable-3.1/pipeline.svg)](https://git.ispconfig.org/ispconfig/ispconfig3/commits/stable-3.1)


- Manage multiple servers from one control panel
- Web server management (Apache2 and nginx)
- Mail server management (with virtual mail users)
- DNS server management (BIND and MyDNS)
- Virtualization (OpenVZ)
- Administrator, reseller and client login
- Configuration mirroring and clusters
- Open Source software (BSD license)

## Added features by Nuclear.Hosting

- ISPConfig Clean SSL Private keys from database (cb9011b45146102cdc8faf3c09b48a1dd4cb7ecb)
- ISPConfig logs logrotate ( c2b2d1a6c62eaf4eb000681d37f7cbf8286796fb )
- ISPConfig new API functions: mail_catchall_get_all, mail_user_get_all, sites_database_user_get_by_name, sites_web_domainid_get (9c34d193ab5ddc80ea41a7e6f01e4b27e527fc98)
- ISPConfig new API functions: client_enable_all_services, client_disable_all_services (68c3d84bcf70eb0641c6f7a21556ceafe421bd55)