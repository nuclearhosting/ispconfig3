<?php

class cronjob_nh_clean_private_keys extends cronjob {

	// job schedule
	protected $_schedule = '0 23 * * *';
	
    /* this function is optional if it contains no custom code */
        public function onPrepare() {
                global $app;

                parent::onPrepare();
	}
	
        /* this function is optional if it contains no custom code */
        public function onBeforeRun() {
                global $app;

                return parent::onBeforeRun();
	}
	
        public function onRunJob() {
            global $app, $conf;

            $app->log('Deleting SSL private keys from DB', LOGLEVEL_DEBUG);
            $domains = $app->db->queryAllRecords("SELECT domain FROM web_domain WHERE (ssl_action IS NULL or ssl_action = '') AND ssl_key IS NOT NULL AND `ssl`='y'");
            
            if(is_array($domains)) {
                $app->log('For these domains will be deleted SSL private keys from DB: '.print_r($domains, true), LOGLEVEL_DEBUG);
                $app->db->query("UPDATE web_domain SET ssl_key = NULL WHERE (ssl_action IS NULL or ssl_action = '') AND ssl_key IS NOT NULL AND `ssl`='y'");
            }

            parent::onRunJob();
        }


        /* this function is optional if it contains no custom code */
        public function onAfterRun() {
                global $app;

                parent::onAfterRun();
        }

}

?>

